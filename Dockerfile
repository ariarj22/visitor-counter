# specify a base image
FROM node:alpine

# copy folder and file
COPY ./package.json ./

# install some dependencies
RUN npm install

COPY ./ ./

# specify startup command
CMD ["npm", "start"]